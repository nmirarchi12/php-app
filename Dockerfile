FROM php:apache

EXPOSE 80
RUN apt-get update && apt-get install -y vim && apt-get install -y iputils-ping && apt-get install -y dnsutils
RUN docker-php-ext-install mysqli
RUN docker-php-ext-enable mysqli
RUN apachectl restart
RUN mkdir /myproject
COPY index.php /myproject
COPY delete_form.php /myproject
COPY contact.php /myproject
COPY contact_delete.php /myproject
COPY insert_form.html /myproject
WORKDIR /myproject
CMD ["php", "-S", "0.0.0.0:80"]                                   
